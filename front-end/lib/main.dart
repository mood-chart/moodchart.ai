import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:core';
import 'package:random_string_generator/random_string_generator.dart';


void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Retrieve Text Input',
      home: MyCustomForm(),
    );
  }
}

// Define a custom Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  State<MyCustomForm> createState() => _MyCustomFormState();
}

// Define a corresponding State class.
// This class holds the data related to the Form.
class _MyCustomFormState extends State<MyCustomForm> {
  // Create a text controller and use it to retrieve the current value
  // of the TextField.
  final myController = TextEditingController();
  
	Future<String> SendHTTP(String title) async {
		var response = await http.post(
		  Uri.parse('http://3.24.21.230'),
		  body: {"text field": title}
		);
		return(response.body);
	}	

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }


 	bool valid_email(String emailadd)
  {
    List<String> listsplit = emailadd.split('@');
    if (listsplit.length > 1){
      return(true);

    }else{
      return(false);
    }
  }
 	Future<String> get _localPath async {
		final directory = await getApplicationDocumentsDirectory();
		return directory.path;
	}
 	Future<String> Datafile(bool randstring) async {
    final filepath = await _localPath;
    String ffile = "";
    if (randstring == true){
      ffile = 'randstring13.txt';
    }else{
      ffile = "email13.txt";
    }
    final file = File('$filepath/$ffile');
    String line = "";
    Stream<String> lines = file.openRead()
        .transform(utf8.decoder) // Decode bytes to UTF-8.
        .transform(LineSplitter()); // Convert stream to individual lines.
    try {
      await for (var line in lines) {
        return(line);
       }
       return(line);
    } catch (e) {
      return (line);
    }
  }
  

  void writeFile(String rand, bool randstring) async {
    final path = await _localPath;
    String ffile = "";
    if (randstring == true){
      ffile = "randstring13.txt";
    }else{
      ffile = "email13.txt";
    }
    final filename = '$path/$ffile';
    var file = File(filename).writeAsString(rand);
    // Do something with the file.
  }

  String randomstring(){
    var generator = RandomStringGenerator(
    minLength: 5,
    maxLength: 25,
    );

    var string = generator.generate();
    return(string);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Describe mood in words'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: TextField(
          controller: myController,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        // When the user presses the button, show an alert dialog containing
        // the text that the user has entered into the text field.

        onPressed: () async {
          String alertemail = "We don't have your email on file, please input it into the input text field";
          print("test");
          String inemail = await Datafile(false);

          if (valid_email(inemail) == false && valid_email(myController.text) == false){
            alertemail = "We don't have your email on file, please input it into the input text field";
          }else if (valid_email(myController.text) == true) {
            alertemail = "We sent random letters to authenticate, please check your email";
            SendHTTP(myController.text);
            writeFile(myController.text, false);
            print("what the fuck");

          }
          String randin = await Datafile(true);
          String emailin = await Datafile(false);

          if (valid_email(emailin) == true && valid_email(myController.text) == false && myController.text.split("Randstr").length <= 1){
            print("mang");
            SendHTTP(myController.text+"randin:"+randin);
            alertemail = "We'll send sentiment of mood, if you're in the database";

          }if (myController.text.split("Randstr").length > 1){
            alertemail = "We'll try to authenticate you now";
            SendHTTP(myController.text);
            writeFile(myController.text, true);
          }

          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                // Retrieve the text the that user has entered by using the
                // TextEditingController.
                content: Text(alertemail),
              );
            },
          );
        },
        tooltip: 'Show me the value!',
        child: const Icon(Icons.text_fields),
      ),
      
    );
  }
}

