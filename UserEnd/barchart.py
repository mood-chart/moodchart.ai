import matplotlib.pyplot as plt
import numpy as np
import time


def barchart(dates, data):
    #bar chart of positivity]
    print(data,"mang")
    timesec = int(time.time())
    pos = np.arange(len(dates))
    if data[0] > 0.5:
        plt.bar(pos,data,color='blue',edgecolor='black')
    elif data[0] < 0.5:
        plt.bar(pos,data,color='red', edgecolor='black')

    plt.xticks(pos, dates)
    plt.xlabel('Days', fontsize=16)
    plt.ylabel('Positivity', fontsize=16)
    plt.title('Positivity over days',fontsize=20)
    plt.savefig(str(timesec))
    plt.clf()
    del data
    return(str(timesec)+'.png')
