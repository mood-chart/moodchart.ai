from exchangelib import Credentials, Account, Configuration, DELEGATE, Message, Mailbox

def connect(server, email, username, password):
    """
    Get Exchange account connection with server
    """
    creds = Credentials(username=username, password=password)
    config = Configuration(server=server, credentials=creds)
    return Account(primary_smtp_address=email, autodiscover=False,
                   config = config, access_type=DELEGATE)


#email user of data link S3 Bucket
def email_link(link):
    # Connection details
    server = 'mail.premium.exchange'
    email = 'emmanuel@moodchart.ai'
    username = ''
    password = '' # prompt for password
    a = connect(server, email, username, password)

 
    m = Message(

            account=a,

            subject='Link to file',

            body='Here is the link to your chart %s'%link,

            to_recipients=[

            Mailbox(email_address='emmanuel@inferencetraining.ai'),

            ] # Or a mix of both

		)
    m.send()


