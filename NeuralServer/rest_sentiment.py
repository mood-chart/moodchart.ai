from flask import Flask, redirect, url_for, request
from sentiment import sent


app = Flask(__name__)

@app.route("/", methods = ['POST', 'GET'])
def sentiment():
   if request.method == 'POST':
    text_input = request.form['text_input']
    senti = sent(text_input)
    return(str(senti))
